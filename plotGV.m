function [hPlot] = plotGV(plasma, results, solution, data, VarL, tmp_DF, tmp_DDF, Text)
    SizeR = size(results);
    if length(data.ky)>1 && length(data.kx)>1
        Rplot = reshape(results(1,:,:), SizeR(2:3));
        hPlot = PlotGV3D( plasma,  data, Rplot, Text);
    else
        hPlot = PlotGV2D( plasma, data, solution, VarL, tmp_DF, tmp_DDF, Text);
    end
end

