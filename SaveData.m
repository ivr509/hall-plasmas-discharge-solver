function [ ] = SaveData( plasma, results, solution, VarL, data, FileName)
PLASMA_FIELDS;
VarName = {'Parameter'};
Plasma = Fields;
Param = '';
for i =1:length(DFields)
    if length(data.(DFields{i}))>1
        Param = DFields{i};
    end
end
for i = 1:length(Fields)
    for Var = 1:length(plasma.(Fields{i}))
        Plasma{Var + 1, i} = plasma.(Fields{i})(Var);
        VarName{Var + 1} = ['case' num2str(Var)];
    end
end
for Var = 1:VarL
    SizeR = size(results);
    if length(SizeR)>2
        Rplot = reshape(results(Var,:,:), SizeR(2:3));
    else
        Rplot = results(Var,:,:);
    end
    if length(data.ky)>1 && length(data.kx)>1
        [~, ind] = max(imag(Rplot(:)));
        [I_row, I_col] = ind2sub(size(Rplot),ind);
    else
        [~,indMax] = max(imag(results(Var,:,:)));
        if length(data.ky)>1
            I_row = 1;
            I_col = indMax;
        end
        if length(data.kx)>1
            I_row = indMax;
            I_col = 1;
        end
    end
    if length(data.ky)>1
        plasma.ky(Var) = data.ky(I_col);
    end
    if length(data.kx)>1
        plasma.kx(Var) = data.kx(I_row);
    end
    plasma.Omega(Var) = (results(Var, I_row, I_col));
end
if length(data.ky)>1
    i = i + 1;
    Plasma{1, i} = 'Max k_y';
	for Var = 1:length(plasma.ky)
        Plasma{Var + 1, i} = plasma.ky(Var);
    end  
end
if length(data.kx)>1
    i = i + 1;
    Plasma{1, i} = 'Max k_x';
    for Var = 1:length(plasma.kx)
        Plasma{Var + 1, i} = plasma.kx(Var);
    end
end
i = i + 1;
Plasma{1, i} = 'Max omega';
for Var = 1:length(plasma.Omega)
    Plasma{Var + 1, i} = plasma.Omega(Var);
end
i = i + 1;
Plasma{1, i} = 'Fluid';
Plasma{2, i} = data.fluid;
T = cell2table(Plasma','VariableNames',VarName);
writetable(T,[FileName '_plasma.txt'],'Delimiter','\t','WriteRowNames',true);

for Var = 1:VarL
    for KY = 1:length(data.ky)
        for KX = 1:length(data.kx)
            [~, ind] = sort(imag(solution(:,Var,KX,KY)));
            for i = 1:length(ind)
                root(i,Var,KX,KY) = solution(ind(i),Var,KX,KY);
            end
        end
    end
end
t = 1;
for Var = 1:VarL
    for KY = 1:length(data.ky)
        for KX = 1:length(data.kx)
            DATA{t, 1} = Param;
            DATA{t, 2} = data.ky(KY);
            DATA{t, 3} = data.kx(KX);
            for i = 1:size(root, 1)
                DATA{t, 3 + i} = root(i,Var,KX,KY);
            end
            t = t + 1;
        end
    end
end
if size(root, 1)>2
    VarNames = {'Parameter'; 'KY'; 'KX'; 'ROOT1'; 'ROOT2'; 'ROOT3'};
else
    VarNames = {'Parameter'; 'KY'; 'KX'; 'ROOT1'; 'ROOT2'};
end

T1 = cell2table(DATA,'VariableNames',VarNames);
writetable(T1,[FileName '_data.txt'],'Delimiter','\t','WriteRowNames',true);