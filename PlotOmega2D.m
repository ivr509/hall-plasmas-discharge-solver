function [ hPlot ] = PlotOmega2D( plasma, data, solution, VarL, tmp_DF, tmp_DDF, Text)
    for Var = 1:VarL
        for KY = 1:length(data.ky)
            for KX = 1:length(data.kx)
                [~, ind] = sort(imag(solution(:,Var,KX,KY)));
                t = 1;
                if imag(solution(ind(end),Var,KX,KY)) > 0
                    if real(solution(ind(end),Var,KX,KY)) > 0
                        root(t,Var,KX,KY) = solution(ind(end),Var,KX,KY);
                    end
                else
                    [~, ind] = sort(real(solution(:,Var,KX,KY)));
                    root(t,Var,KX,KY) = solution(ind(end),Var,KX,KY);
                end
           end
        end
    end
    style = ['s';'o';'x';'+';'*';'p'];
    
    if length(data.ky)>1
        k = data.ky;
        k_type = 'k_y';
    end
    if length(data.kx)>1
        k = data.kx;
        k_type = 'k_x';
    end
    RootS = size(root);
    SizeN = prod(RootS(3:end)); 
    cmap = ['r';'b';'m'];
    for Var=1:VarL
        t = 1;
        tt = 1;
        clear legd;
        hPlot(Var) = figure('NumberTitle' ,'off', 'Tag', 'hPlot', 'Name', 'Frequency', 'Resize', 'Off',...
                'units','centimeters','outerposition',[2 2 20.5 15.5],'PaperPositionMode','auto', 'Visible', 'on');
        hold on;
        if length(plasma.re)>1
            Re = plasma.re(Var);
        else
            Re = plasma.re;
        end
        for i = 1:RootS(1)
            Rplot = reshape(root(i,Var,:,:), [1 SizeN]);
            F = length(Rplot);
            plot(abs(k(1:F)).*Re, real(Rplot), '-', 'Color', cmap(i), 'Linewidth', 2);
            plot(abs(k(1:F)).*Re, imag(Rplot), '--', 'Color', cmap(i), 'Linewidth', 2);
            if ~isempty(tmp_DF)
                legd{:,t} = ['Re(\omega_' num2str(i) '), ' tmp_DF ' = ' num2str(data.(tmp_DF)(Var)) ' ' tmp_DDF];
            else
                legd{:,t} = ['Re(\omega_' num2str(i) ')'];
            end
            t = t + 1;
            legd{:,t} = ['Im(\omega_' num2str(i) ')'];
            t = t + 1;
            M(tt) = max(abs(real(Rplot)));
            tt = tt + 1;
            M(tt) = max(abs(imag(Rplot)));
        end
        plot(k(1:F).*plasma.re, k(1:F).*plasma.cs, 'k-', 'linewidth', 3);
        if length(data.kx)>1
            plot(abs(k(1:F)).*Re, k(1:F).*plasma.v0, 'g-', 'linewidth', 3);
        else
            plot(abs(k(1:F)).*Re, k(1:F).*plasma.v0, 'g-', 'linewidth', 3);
        end
        Max = max(M);
        LX = get(gca,'XLim');
        LY = get(gca,'YLim');
        axis([LX(1) LX(2) LY(1) Max]);
        legd{:,t} = [k_type 'c_s'];
        t = t + 1;
        legd{:,t} = [k_type 'v_0'];
        if ~isempty(tmp_DF)
            hleg = legend(legd,'Units','centimeters','Position',[14 8.5 5 2]);
        else
            hleg = legend(legd,'Units','centimeters','Position',[12.5 8.5 5 2]);
        end
        set(hleg,'FontUnits','points',...
                'FontWeight','normal',...
                'FontSize',Text.leg,...
                'FontName','Times');
        xlabel([k_type '\rho_e'],'FontUnits','points',...
                'FontWeight','normal',...
                'FontSize',Text.text,...
                'FontName','Times');
        ylabel('\omega , s^{-1}','FontUnits','points',...
                'FontWeight','normal',...
                'FontSize',Text.text,...
                'FontName','Times');
        set(gca,...
            'Units','centimeters',...
            'Position',[3 2 10 10],...
            'FontUnits','points',...
            'FontWeight','normal',...
            'FontSize',Text.axis,...
            'FontName','Times');
        axis square;
        grid on;
        legend('boxoff');
    end