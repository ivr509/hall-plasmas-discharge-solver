function [ hPlot ] = PlotOmega3D( plasma, data, solution, VarL, Text, type)
    for Var = 1:VarL
        for KY = 1:length(data.ky)
            for KX = 1:length(data.kx)
                [~, ind] = sort(imag(solution(:,Var,KX,KY)));
                t = 1;
                if imag(solution(ind(end),Var,KX,KY)) > 0
                    if real(solution(ind(end),Var,KX,KY)) > 0
                        root(t,Var,KX,KY) = solution(ind(end),Var,KX,KY);
                    end
                else
                    [~, ind] = sort(real(solution(:,Var,KX,KY)));
                    root(t,Var,KX,KY) = solution(ind(end),Var,KX,KY);
                end
           end
        end
    end
    RootS = size(root);
    SizeN = prod(RootS(3:end));
    Rplot = reshape(root(1,1,:,:), [RootS(end-1) RootS(end)]);
    switch type
        case 'real'
            hPlot = figure('NumberTitle' ,'off', 'Tag', 'hPlot', 'Name', 'Frequency', 'Resize', 'Off', ...
                'units','centimeters','outerposition',[2 2 20 15],'PaperPositionMode','auto', 'Visible', 'on');
            contourf(plasma.re.*data.ky, plasma.re.*data.kx, real(Rplot), 'LineWidth', 2);
        case 'imag'
            hPlot = figure('NumberTitle' ,'off', 'Tag', 'hPlot', 'Name', 'Growth Rate', 'Resize', 'Off', ...
                'units','centimeters','outerposition',[2 2 20 15],'PaperPositionMode','auto', 'Visible', 'on');
            contourf(plasma.re.*data.ky, plasma.re.*data.kx, imag(Rplot), 'LineWidth', 2);
    end
    colorbar
    grid on;
    axis square;
    xlabel('k_y\rho_e','FontUnits','points',...
            'FontWeight','normal',...
            'FontSize',Text.text,...
            'FontName','Times');
    ylabel('k_x\rho_e','FontUnits','points',...
            'FontWeight','normal',...
            'FontSize',Text.text,...
            'FontName','Times'); 
    set(gca,...
            'FontUnits','points',...
            'FontWeight','normal',...
            'FontSize',Text.axis,...
            'FontName','Times');
end

