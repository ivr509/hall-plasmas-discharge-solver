%% STRINGS FOR PLASMA PARAMS
s{1} = 'Magnetic field, $G$';
s{2} = 'Electric field, $V/m$';
s{3} = '$T_e$, $eV$';
s{4} = 'Density, $m^{-3}$';
s{5} = '$\Delta\phi$, $V$';
s{6} = 'Density gradient length, $m$';
s{7} = 'Magnetic gradient length, $m$';
s{8} = 'Collision freq, $s^{-1}$';
s{9} = 'Wavenumber $k_y$, $m^{-1}$';
s{10} = 'Wavenumber $k_x$, $m^{-1}$';
s{11} = 'System length in z, $m$';
s{12} = 'Working fluid';
%% TAGS FOR PLASMA PARAMS
Tags{1} = 'B';
Tags{2} = 'E';
Tags{3} = 'Te';
Tags{4} = 'n';
Tags{5} = 'phi';
Tags{6} = 'Ln';
Tags{7} = 'LB';
Tags{8} = 'nu';
Tags{9} = 'ky';
Tags{10} = 'kx';
Tags{11} = 'L';
Tags{12} = 'fluid';
%% EQUATION STRINGS
EQ{1} = 'include $E\times B$';
EQ{2} = 'include density gradient';
EQ{3} = 'include magnetic gradient';
EQ{4} = 'include inertia';
EQ{5} = 'include gyroviscosity';
EQ{6} = 'include collisions';
EQ{7} = 'include 3D effects';
EQ{8} = 'include ion flow';
%% TAGS FOR EQUATION
Tags_EQ{1} = 'w0';
Tags_EQ{2} = 'ws';
Tags_EQ{3} = 'wD';
Tags_EQ{4} = 'in';
Tags_EQ{5} = 'kr';
Tags_EQ{6} = 'coll';
Tags_EQ{7} = 'D3';
Tags_EQ{8} = 'wi';
%% SIZES
Sizes.TextW = 65; % Textbox width
Sizes.TextH = 25; % Textbox heigth
Sizes.ChekW = 25; % Checkbox width
Sizes.ChekH = 25; % Checkbox heigth
Sizes.BoxW = 50;  % Popup width
Sizes.BoxH = 25;  % Popup heigth
Sizes.PushW = 70; % Textbox width
Sizes.PushH = 30; % Textbox heigth
Sizes.EditFL = 200; % Text field length
Sizes.Space = 12.5;

Sizes.C1Bottom = 12.5;
Sizes.C1width = Sizes.Space + Sizes.EditFL + Sizes.Space + Sizes.TextW + Sizes.Space +...
    Sizes.BoxW + Sizes.Space + Sizes.TextW + Sizes.Space + Sizes.ChekW + Sizes.Space;
Sizes.C1Height = (2*length(Tags) + 1)*Sizes.TextH;

Sizes.C2Left = Sizes.Space + Sizes.C1width + Sizes.Space;
Sizes.C2width = Sizes.Space + Sizes.ChekW + Sizes.Space + 1.87*Sizes.EditFL + Sizes.Space;
Sizes.C2Height = (2*length(Tags_EQ) + 3)*Sizes.BoxH;
Sizes.C2Bottom = Sizes.C1Bottom + Sizes.C1Height - Sizes.C2Height;

Sizes.C3Left = Sizes.C2Left;
Sizes.C3width = Sizes.C2width;
Sizes.C3Height = Sizes.C1Height - 12.5 - (2*length(Tags_EQ) + 3)*Sizes.BoxH;
Sizes.C3Bottom = Sizes.C1Bottom;
    
Sizes.WindowH = (2*length(Tags) + 3)*Sizes.TextH; % window heigth
Sizes.WindowW = Sizes.Space + Sizes.C2Left + Sizes.C2width + Sizes.Space; % window width
Sizes.WindowL = 100; % window left corner position
Sizes.WindowB = 100; % window bottom corner position
%% TEXT SIZES
Text.pres = '%4.0f';
Text.text = 15;
Text.title = 15;
Text.leg = 15;
Text.axis = 15;