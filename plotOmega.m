function [hPlot] = plotOmega(plasma, results, solution, data, VarL, tmp_DF, tmp_DDF, Text)
    if length(data.ky)>1 && length(data.kx)>1
        hPlot(1) = PlotOmega3D( plasma, data, solution, VarL, Text, 'real');
        hPlot(2) = PlotOmega3D( plasma, data, solution, VarL, Text, 'imag');
    else
        hPlot = PlotOmega2D( plasma, data, solution, VarL, tmp_DF, tmp_DDF, Text);
    end
end

