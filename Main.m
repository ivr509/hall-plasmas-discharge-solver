function Main
    %% INITIAL VALUES FOR PARAMETERS
    INIT_VALUES;
    %% INITIAL VALUES FOR INTERFACE
    INTERFACE_PARAM;
    f = figure('NumberTitle' ,'off', 'units','pixels','outerposition',[Sizes.WindowL Sizes.WindowB Sizes.WindowW Sizes.WindowH], 'Visible', 'off','MenuBar','none'); % ,'WindowStyle','Modal'
    %% CREATE PLASMA CONTROLS
    handles.bg1 = uibuttongroup(f, 'units','pixels','Visible','on','FontName', 'Times', 'FontSize', 12,'FontAngle', 'italic',...
                  'Title', 'Plasma parameters','Position',[Sizes.Space, Sizes.C1Bottom, Sizes.C1width, Sizes.C1Height]);
	for i = 1:length(Tags)-2
        % check number formatting 
        if initial.(Tags{i})>1E4
            tformat = '%1.1E';
        end
        if initial.(Tags{i})<1E4
            tformat = '%1.0f';
        end
        if initial.(Tags{i})<1
            tformat = '%1.2f';
        end
        if initial.(Tags{i})<0.01
            tformat = '%1.0E';
        end
        % calculate position from top
        bottom = Sizes.C1Height - 2.*Sizes.TextH.*(i);
        % first edit field (start value of the parameter)
        handles.Edit_Fields1(i) = uicontrol(handles.bg1, 'units','pixels','Style','edit','FontName', 'Times', 'FontSize', 12, ...
                'Tag', Tags{i},'String',num2str(initial.(Tags{i}), tformat),'Position',[Sizes.Space + Sizes.EditFL + Sizes.Space, bottom, Sizes.TextW, Sizes.TextH],...
                'Callback',@Editfield_Callback);
        % choose popup menu or edit field for number of points 
        if strcmp(Tags{i},'kx')||strcmp(Tags{i},'ky')
            % edit fields for kx and ky
            handles.Edit_Fields2(i) = uicontrol(handles.bg1, 'units','pixels','Style','popup','FontName', 'Times', 'FontSize', 12, ...
                'Tag', [Tags{i} 'N'],'String',{num2str(initial.([Tags{i} 'N']), '%1.0f'),'300','400','500'},'Position',[Sizes.Space + Sizes.EditFL + Sizes.Space + Sizes.TextW + Sizes.Space, bottom, Sizes.BoxW, Sizes.BoxH],...
                'Callback',@Editfield_Callback, 'Visible', 'off');
        else
            % popup menu for other fields
            handles.Edit_Fields2(i) = uicontrol(handles.bg1, 'units','pixels','Style','popup','FontName', 'Times', 'FontSize', 12, ...
                    'Tag', [Tags{i} 'N'],'String',{num2str(initial.([Tags{i} 'N']), '%1.0f'), '4', '5', '6'},'Position',[Sizes.Space + Sizes.EditFL + Sizes.Space + Sizes.TextW + Sizes.Space, bottom, Sizes.BoxW, Sizes.BoxH],...
                    'Callback',@Editfield_Callback, 'Visible', 'off');
        end
        % second edit field (last value of the parameter)
        handles.Edit_Fields3(i) = uicontrol(handles.bg1, 'units','pixels','Style','edit','FontName', 'Times', 'FontSize', 12, ...
                'Tag', [Tags{i} 'end'],'String',num2str(initial.([Tags{i} 'end']), tformat),'Position',[Sizes.Space + Sizes.EditFL + Sizes.Space + Sizes.TextW + Sizes.Space + Sizes.BoxW + Sizes.Space, bottom, Sizes.TextW, Sizes.TextH],...
                'Callback',@Editfield_Callback, 'Visible', 'off');
        % checkbox to choose what parameter to evaluate
        handles.Chk_box(i) = uicontrol(handles.bg1, 'units','pixels','Style','checkbox',...
                'Tag', [Tags{i} '_k' num2str(i)], 'Position',[Sizes.Space + Sizes.EditFL + Sizes.Space + Sizes.TextW + Sizes.Space + Sizes.BoxW + Sizes.Space + Sizes.TextW + Sizes.Space, bottom, Sizes.ChekW, Sizes.ChekH],...
                'Callback',@ChkBox_Callback); 
	end
    % create control for system length
    i = i + 1;
    % check number formatting 
    if initial.(Tags{i})>1E4
        tformat = '%1.1E';
    end
    if initial.(Tags{i})<1E4
        tformat = '%1.0f';
    end
    if initial.(Tags{i})<1
        tformat = '%1.2f';
    end
    if initial.(Tags{i})<0.01
        tformat = '%1.0E';
    end
    bottom = Sizes.C1Height - 2.*Sizes.TextH.*(i);
    handles.Edit_Fields1(i) = uicontrol(handles.bg1, 'units','pixels','Style','edit','FontName', 'Times', 'FontSize', 12, ...
                'Tag', Tags{i},'String',num2str(initial.(Tags{i}), tformat),...
                'Position',[Sizes.Space + Sizes.EditFL + Sizes.Space, bottom, Sizes.TextW, Sizes.TextH],...
                'Callback',@Editfield_Callback);
    handles.laxis = axes('parent',handles.bg1,'units','normalized','position',[0 0 1 1],'visible','off');
    % create control to select gas
    i = i + 1;
    bottom = Sizes.C1Height - 2.*Sizes.TextH.*(i);
    handles.Edit_Fields2(i) = uicontrol(handles.bg1, 'units','pixels','Style','popupmenu',...
                   'FontName', 'Times', 'FontSize', 12,'String',{'Xe','Ar', 'He'},'Tag', Tags{i},...
                   'Position',[Sizes.Space + Sizes.EditFL + Sizes.Space, bottom, Sizes.BoxW,Sizes.BoxH]);
    handles.laxis = axes('parent',handles.bg1,'units','normalized','position',[0 0 1 1],'visible','off');
    % Crete text fields with latex.
    for i = 1:length(handles.Edit_Fields1)
        set(handles.Edit_Fields1(i),'units','normalized');
        p = get(handles.Edit_Fields1(i),'position');
        handles.Text_Fields(i) = text(p(1) - (Sizes.EditFL + Sizes.Space)/Sizes.C1width, p(2) + 0.5.*p(4), s{i},'interpreter','latex');
        handles.Text_Fields(i).Color = 'k';
        handles.Text_Fields(i).FontSize = 12;
        handles.Text_Fields(i).FontWeight = 'bold';
    end
    i = i + 1;
    set(handles.Edit_Fields2(end),'units','normalized');
    p = get(handles.Edit_Fields2(end),'position');
    handles.Text_Fields(i) = text(p(1) - (Sizes.EditFL + Sizes.Space)/Sizes.C1width, p(2) + 0.5.*p(4), s{i},'interpreter','latex');
	handles.Text_Fields(i).Color = 'k';
	handles.Text_Fields(i).FontSize = 12;
	handles.Text_Fields(i).FontWeight = 'bold';
    %% CREATE EQUATION CONTROLS
    handles.bg2 = uibuttongroup(f, 'units','pixels','Visible','on','FontName', 'Times', 'FontSize', 12,'FontAngle', 'italic',...
                  'Title', 'Equation type','Position',[Sizes.C2Left, Sizes.C2Bottom, Sizes.C2width, Sizes.C2Height]);
    handles.laxis2 = axes('parent',handles.bg2,'units','normalized','position',[0 0 1 1],'visible','off');
    for i = 1:length(Tags_EQ)
        bottom_EQ = Sizes.C2Height - 2.*Sizes.ChekH.*(i);
        handles.Eq_Fields(i) = uicontrol(handles.bg2, 'units','pixels','Style','checkbox',...
            'Tag', Tags_EQ{i}, 'Position',[Sizes.Space, bottom_EQ, Sizes.ChekW, Sizes.ChekH],'Callback',@EQ_Callback, 'Value', param.(Tags_EQ{i}));
        set(handles.Eq_Fields(i),'units','normalized');
        p = get(handles.Eq_Fields(i),'position');
        handles.Text_EQ(i) = text(p(1) + (Sizes.ChekW + Sizes.Space)/Sizes.C2width, p(2) + 0.5.*p(4), EQ{i},'interpreter','latex');
        handles.Text_EQ(i).Color = 'k';
        handles.Text_EQ(i).FontSize = 12;
        handles.Text_EQ(i).FontWeight = 'bold';
    end
    ss = ['$\frac{\omega_*+k_{\perp}^2\rho_e^2(\omega-\omega_0+i\nu_e) + ik_{\parallel}^2D_{\parallel}}' ...
        '{\omega-\omega_0+k_{\perp}^2\rho_e^2(\omega-\omega_0 +i\nu_e) + ik_{\parallel}^2D_{\parallel}}' ...
        '= \frac{k_{\perp}^2c_s^2 + k_{\parallel}^2c_s^2}{\omega^2}$'];
    handles.t = text(p(1), p(2) - 1.5.*p(4), ss,'interpreter','latex','Visible', 'on', 'Parent', handles.laxis2);
    handles.t.Color = 'k';
    handles.t.FontSize = 19;
    handles.t.FontWeight = 'bold';
    handles.t.BackgroundColor = 'w';
  
    %% CREATE SOLVER CONTROLS
    handles.bg3 = uibuttongroup(f, 'units','pixels','Visible','on','FontName', 'Times', 'FontSize', 12,'FontAngle', 'italic',...
                  'Position',[Sizes.C3Left, Sizes.C3Bottom, Sizes.C3width, Sizes.C3Height]);
    handles.PB(1) = uicontrol(handles.bg3, 'units','pixels','Style','pushbutton','Tag', 'Calculate',...
        'Position',[Sizes.Space, Sizes.C3Height - Sizes.Space - Sizes.PushH, Sizes.PushW, Sizes.PushH],...
        'FontName', 'Times', 'FontSize', 12,'String','Calculate','Callback',@PB_Callback);
    handles.PB(2) = uicontrol(handles.bg3, 'units','pixels','Style','pushbutton','Tag', 'Plot', ...
        'Position',[Sizes.Space, Sizes.C3Height - 2*Sizes.Space - 2*Sizes.PushH, Sizes.PushW, Sizes.PushH],...
        'FontName', 'Times', 'FontSize', 12,'String','Plot','Callback',@PB_Callback);  
    handles.PB(3) = uicontrol(handles.bg3, 'units','pixels','Style','pushbutton','Tag', 'PlotGV', ...
    'Position',[2*Sizes.Space + Sizes.PushW, Sizes.C3Height - 2*Sizes.Space - 2*Sizes.PushH, Sizes.PushW, Sizes.PushH],...
        'FontName', 'Times', 'FontSize', 12,'String','Plot GV','Callback',@PB_Callback);    
    handles.PB(4) = uicontrol(handles.bg3, 'units','pixels','Style','pushbutton','Tag', 'Save',...
        'Position',[Sizes.Space, Sizes.C3Height - 3*Sizes.Space - 3*Sizes.PushH, Sizes.PushW, Sizes.PushH],...
        'FontName', 'Times', 'FontSize', 12,'String','Save','Callback',@PB_Callback);
    handles.File_Fields(1) = uicontrol(handles.bg3, 'units','pixels','Style','edit','FontName', 'Times',...
    'Position', [Sizes.C3width-Sizes.TextW-Sizes.Space Sizes.C3Height-Sizes.Space-Sizes.TextH Sizes.TextW Sizes.PushH],...
        'FontSize', 12,'Tag', 'FileName', 'String','Fig1','Callback',@Editfield_Callback);
    handles.laxis3 = axes('parent',handles.bg3,'units','normalized','position',[0 0 1 1],'visible','off');
    set(handles.File_Fields,'units','normalized');
	p = get(handles.File_Fields,'position');
    handles.Text_Fields(end+1) = text(p(1), p(2) + 0.5.*p(4), 'Figure name','interpreter','latex', 'Parent', handles.laxis3);
    p2 = get(handles.Text_Fields(end), 'Extent');
    handles.Text_Fields(end).Position = [p(1) - 1.5*p2(3), p(2) + 0.5.*p(4)];
    handles.Text_Fields(end).Color = 'k';
    handles.Text_Fields(end).FontSize = 12;
    handles.Text_Fields(end).FontWeight = 'bold';
    %% Initialize the UI.
    f.Name = 'Dispertion tool v.2.0';
    f.ToolBar  = 'none';
    f.Resize  = 'off';
    movegui(f,'center')
    f.Visible = 'on';
    handles.param = param;
    handles.Text = Text;
    handles.initial = initial;
    handles.Numel = length(Tags) - 2;
    handles.Tags = Tags;
    handles.Total = 0;
    handles.Tags_EQ = Tags_EQ;
    guidata(f,handles);
end
function ChkBox_Callback(hObject, eventdata, handles)
    handles = guidata(gcbo);
    Tag = regexp(hObject.Tag,'\d*','Match');
    Tag = str2double(Tag{1});
    if get(hObject,'Value')
        set(handles.Edit_Fields2(Tag),'Visible','On');
        set(handles.Edit_Fields3(Tag),'Visible','On');
        if Tag<handles.Numel-1
            handles.Total = handles.Total + 1;
        else
            handles.Total = handles.Total + 2;
        end
    else
        set(handles.Edit_Fields2(Tag),'Visible','Off');
        set(handles.Edit_Fields3(Tag),'Visible','Off');
        if Tag<handles.Numel-1
            handles.Total = handles.Total - 1;
        else
            handles.Total = handles.Total - 2;
        end
    end
    if handles.Total == 0
        set(handles.Chk_box(1:handles.Numel),'Enable','On');
    end
    if handles.Total == 1
        for i = 1:handles.Numel-2
            tmp = get(handles.Chk_box(i),'Value');
            if tmp == 0
                set(handles.Chk_box(i),'Enable','Off');
            end
        end  
        set(handles.Chk_box(handles.Numel-1:handles.Numel),'Enable','On');
    end
    if handles.Total == 2
        set(handles.Chk_box(1:handles.Numel),'Enable','On');
    end
    if handles.Total == 3
        for i = 1:handles.Numel
            tmp = get(handles.Chk_box(i),'Value');
            if tmp == 0
                set(handles.Chk_box(i),'Enable','Off');
            end
        end    
    end
    if handles.Total == 4
        set(handles.Chk_box(1:handles.Numel-2),'Enable','Off');   
    end
    guidata(hObject,handles);
end
function EQ_Callback(hObject, eventdata, handles)
    handles = guidata(gcbo);
    b=hObject.Value;
    handles.param.(hObject.Tag) = double(b);
    w0 = handles.param.w0;
    ws = handles.param.ws;
    wD = handles.param.wD;
    in = handles.param.in;
    wi = handles.param.wi;
    coll = handles.param.coll;
    D3 = handles.param.D3;
    GV = handles.param.kr;

    omega0 = '';
    omegas = '';
    omegaD = '';
    gyro = '';
    nu = '';
    inert = '';
    dim3 = '';
    cs = 'k_{\perp}^2c_s^2';
    
    omega = '\omega^2}';
    if w0
        omega0 = '-\omega_0';
    end
    if ws
        omegas = '\omega_*';
    end
    if wD
        omegaD = '-\omega_D';
    end
    if coll
        nu = '+i\nu_e';
    end
    if in
        inert = ['k_{\perp}^2\rho_e^2(\omega' omega0 nu ')'];
    end
    if D3
        dim3 = ' + ik_{\parallel}^2D_{\parallel}';
        cs = [cs ' + k_{\parallel}^2c_s^2'];
    end
    if GV
        gyro = [' + k_{\perp}^2\rho_e^2(\omega' omega0 nu ')'];
    end
    if wi
        omega = '(\omega - \omega_i)^2}';
    end
    if ws && in && wD
        omegas = [omegas omegaD '+'];
    end
    if ws && in && ~wD
        omegas = [omegas '+'];
    end
    if ~ws && in && wD
        omegas = [omegaD '+'];
    end
    if ws && ~in && wD
        omegas = [omegas omegaD];
    end
    if ~ws && ~in && wD
        omegas = [omegaD];
    end
    if ~ws && ~in && ~wD
        omegas = '1';
    end
    ss = ['$\frac{' omegas inert dim3 ' }{\omega' omega0 omegaD gyro dim3 '}= \frac{' cs '}{' omega '$'];
    delete(handles.t);
    set(handles.Eq_Fields(end),'units','normalized');
 	p = get(handles.Eq_Fields(end),'position');
    handles.t = text(p(1), p(2) - 1.5.*p(4), ss,'interpreter','latex','Visible', 'on', 'Parent', handles.laxis2);
    handles.t.Color = 'k';
    handles.t.FontSize = 19;
    handles.t.FontWeight = 'bold';
    handles.t.BackgroundColor = 'w';
    guidata(hObject,handles);
end
function Editfield_Callback(hObject, eventdata, handles)
    handles = guidata(gcbo);
    if strcmp(hObject.Tag, 'FileName')
        if isempty(strtrim(hObject.String))
            msgbox('Please enter filename','Warning','warn','modal','FontSize', 24);
            hObject.String = 'Fig1';
            return
        end
    else
        if isnan(str2double(hObject.String))
            msgbox('Please enter numeric value','Warning','warn','modal','FontSize', 24);
            hObject.String = num2str(handles.initial.(hObject.Tag));
            return
        end
    end
    guidata(hObject,handles);
end

function PB_Callback(hObject, eventdata, handles)
    handles = guidata(gcbo);
    Tag = hObject.Tag;
    switch Tag
        case 'Calculate'
            clear handles.plasma handles.results handles.solution data;
            if (handles.param.wi + handles.param.w0 + handles.param.ws + handles.param.kr + handles.param.in + handles.param.coll+ handles.param.D3)<2
                msgbox('Equation should include at least two parameters');
                return;
            end
            if ~get(handles.Chk_box(end),'Value') && ~get(handles.Chk_box(end-1),'Value')
                msgbox('Choose kx or ky parameter');
                return;
            end
            kx1 = str2double(get(handles.Edit_Fields1(end),'String'));
            kx2 = str2double(get(handles.Edit_Fields3(end),'String'));
            ky1 = str2double(get(handles.Edit_Fields1(end-2),'String'));
            ky2 = str2double(get(handles.Edit_Fields3(end-1),'String'));
            if handles.Chk_box(end).Value
                if kx1 >= kx2
                    msgbox('Final kx cannot be less than first one');
                    return;
                end
            end
            if handles.Chk_box(end-1).Value
                if ky1 >= ky2
                    msgbox('Final ky cannot be less than first one');
                    return;
                end
            end
            for i = 1:handles.Numel
                tmp1 = str2double(get(handles.Edit_Fields1(i),'String'));
                if handles.Chk_box(i).Value
                    selectedIndex = handles.Edit_Fields2(i).Value;
                    tmp2 = str2double(get(handles.Edit_Fields2(i),'String'));
                    tmp2 = tmp2(selectedIndex);
                    tmp3 = str2double(get(handles.Edit_Fields3(i),'String'));
                    data.(handles.Tags{i}) = linspace(tmp1, tmp3, tmp2);
                else
                    data.(handles.Tags{i}) = tmp1;
                end
            end
            tmpL = str2double(get(handles.Edit_Fields1(end),'String'));
            tmpf = get(handles.Edit_Fields2(end),'String');
            selectedIndex = handles.Edit_Fields2(end).Value;
            data.fluid = tmpf{selectedIndex};
            data.L = tmpL;
            data.Ti = 0;
            handles.data = data;
            [handles.plasma, handles.results, handles.solution, handles.VarL, handles.tmp_DF, handles.tmp_DDF]=calc(handles.param, data);
        case 'Plot'
            if isfield(handles, 'hPlot')
                for i = 1:length(handles.hPlot)
                    if isvalid(handles.hPlot(i))
                        close(handles.hPlot(i));
                    else
                        delete(handles.hPlot(i));
                    end
                end
            end
            if isfield(handles, 'plasma')
                handles.hPlot = plotOmega(handles.plasma, handles.results, handles.solution, handles.data, handles.VarL, handles.tmp_DF, handles.tmp_DDF, handles.Text);
            else
                msgbox('Run calculations first');
            end
        case 'PlotGV'
             if isfield(handles, 'hPlotGV')
                for i = 1:length(handles.hPlotGV)
                    if isvalid(handles.hPlotGV(i))
                        close(handles.hPlotGV(i));
                    else
                        delete(handles.hPlotGV(i));
                    end
                end
            end
            if isfield(handles, 'plasma')
                handles.hPlotGV = plotGV(handles.plasma, handles.results, handles.solution, handles.data, handles.VarL, handles.tmp_DF, handles.tmp_DDF, handles.Text);
            else
                msgbox('Run calculations first');
            end
        case 'Save'
            if isfield(handles, 'plasma')
                SaveData(handles.plasma, handles.results, handles.solution, handles.VarL, handles.data, handles.File_Fields(1).String);
            else
                msgbox('Run calculations first');
            end
    end
    guidata(hObject,handles);
end