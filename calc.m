function [tmp_plm, results, solution, VarL, tmp_DF, tmp_DDF] = calc(param, data) %results, solution
load('constants');
PLASMA_FIELDS;
%% INITIAL VALUES 

switch data.fluid
	case 'Xe'
        data.mi = const.miXe;
	case 'Ar'
        data.mi = const.miAr;
	case 'He'
        data.mi = const.miHe;
end

data.B = data.B.*1E-4; % magnetic field in Gauss
data.E = data.E;

%% velocities
plasma.v0 = data.E./(data.B); % ExB
plasma.vi = sqrt(2*const.q.*data.phi/data.mi); % ion velocity
plasma.vs = -((const.kb.*data.Te.*11604)./(const.q.*data.B))./data.Ln; % electron drift velocity
plasma.vD= -2*((const.kb.*data.Te.*11604)./(const.q.*data.B))./data.LB; % magnetic drift velocity
plasma.cs = sqrt(const.kb.*data.Te.*11604/data.mi); % ion sound
plasma.vTe = sqrt(const.kb.*data.Te.*11604./const.me); % electron thermal velocity
plasma.vTi = sqrt(const.kb.*data.Ti.*11604./data.mi); % ion thermal velocity
%% frequencies
plasma.Wce = (const.q.*data.B)./(const.me);% electron gyrofrequency
plasma.Wci = const.q*data.B/data.mi;% ion cyclotron
plasma.Wpe = sqrt((const.q^2).*data.n./(const.me*const.eps)); %plasma frequency
plasma.Wpi = sqrt((const.q^2).*data.n./(data.mi*const.eps)); %ion plasma frequency
plasma.wLH = (plasma.Wce.*plasma.Wpi)./sqrt(plasma.Wce.^2 +plasma.Wpe.^2); % Low-hybrid frequency
%% lengthes
plasma.re = plasma.vTe./plasma.Wce; % electron gyroradius
plasma.ri = plasma.vTi./plasma.Wci; % ion gyroradius
plasma.rs = plasma.cs./plasma.Wci; % ion-sound Larmor radius
%% collisions
plasma.nu = data.nu; % electron-neutral, s^-1
data.D = const.kb.*data.Te.*11604./(const.me.*data.nu);
plasma.D = data.D;

k = 1;
VarL = 1;
tmp_F = '';
for i = 1:length(Fields)
    if length(plasma.(Fields{i}))>1
        tmp_F{k} = Fields{i};
        VarL = length(plasma.(Fields{i}));
        k = k + 1;
    end
end
tmp_DF = '';
tmp_DDF = '';
for i = 1:length(DFields)
    if length(data.(DFields{i}))>1
        tmp_DF = DFields{i};
        tmp_DDF = DDFields{i};
    end
end
tmp_plm = plasma;
fulllength = length(data.ky).*length(data.kx).*VarL;
h = waitbar(0,'Please wait...');
step = 1;
for Var = 1:VarL
    for i = 1:k-1
        tmp = tmp_plm.(tmp_F{i});
        plasma.(tmp_F{i}) = tmp(Var);
    end
    for KY = 1:length(data.ky)
        for KX = 1:length(data.kx)
            data.kp2(Var, KX,KY) = data.kx(KX).^2 + data.ky(KY).^2;
            K = param.in.*data.kp2(Var, KX,KY).*plasma.re.^2;
            G = param.kr.*data.kp2(Var, KX,KY).*plasma.re.^2;
            P = param.D3.*1i.*plasma.D.*pi.^2./data.L.^2;
            omega0 = param.w0.*data.ky(KY).*plasma.v0;
            omegaS = param.ws.*data.ky(KY).*plasma.vs;
            omegaD = param.wD.*data.ky(KY).*plasma.vD;
            S = data.kp2(Var, KX,KY).*plasma.cs.^2 + param.D3.*plasma.cs.^2.*pi.^2./data.L.^2;
            Omega0 = omega0 + omegaD;
            OmegaS = omegaS - omegaD;
            OmegaNu = -omega0 + param.coll.*1i.*plasma.nu;
            omega_wi = param.wi.*data.kx(KX).*plasma.vi;
            if abs(K)>0
                A(Var, KX,KY) = K;
                B(Var, KX,KY) = OmegaS + OmegaNu.*K - 2.*omega_wi.*K + P;
                C(Var, KX,KY) = -2.*omega_wi.*OmegaS - 2.*omega_wi.*OmegaNu.*K + omega_wi.^2.*K - S - G.*S - 2.*omega_wi.*P;
                D(Var, KX,KY) = OmegaS.*omega_wi.^2 + OmegaNu.*omega_wi.^2.*K + S.*Omega0 + P.*omega_wi.^2 - OmegaNu.*G.*S - P.*S;
                p = [A(Var, KX,KY) B(Var, KX,KY) C(Var, KX,KY) D(Var, KX,KY)]; % 
                solution(:,Var, KX,KY) = roots(p);
            else
                b = (S.*(1 + G)./(2.*(OmegaS + P)) + omega_wi);
                solution(1,Var, KX,KY) = b + sqrt( b.^2 - S.*(1 + G).*omega0./(OmegaS + P) + ...
                    S.*G.*1i.*param.coll.*plasma.nu./(OmegaS + P) - S.*omegaD./(OmegaS + P) + S.*P./(OmegaS + P) - omega_wi.^2);
                solution(2,Var, KX,KY) = b - sqrt( b.^2 - S.*(1 + G).*omega0./(OmegaS + P) + ...
                    S.*G.*1i.*param.coll.*plasma.nu./(OmegaS + P) - S.*omegaD./(OmegaS + P) + S.*P./(OmegaS + P) - omega_wi.^2);
            end
            step = step + 1;
            if rem(step, 20) == 0
                waitbar(step./fulllength);
            end
        end
    end
end
close(h);
for Var = 1:VarL
    for KY = 1:length(data.ky)
        for KX = 1:length(data.kx)
            [~, ind] = sort(imag(solution(:,Var,KX,KY)));
            if imag(solution(ind(end),Var,KX,KY)) > 0
                results(Var,KX,KY) = solution(ind(end),Var,KX,KY);
            else
                [~, ind] = sort(abs(real(solution(:,Var,KX,KY))));
                results(Var,KX,KY) = solution(ind(1),Var,KX,KY);
            end
        end
    end
end

f = figure('NumberTitle' ,'off', 'units','pixels','outerposition',[100 100 350 600], 'Visible', 'on','MenuBar','none');
handles.laxis = axes('parent',f,'units','normalized','position',[0 0 1 1],'visible','off');
px = 0.025;
for i = 1:length(Fields)
    tt = '';
    py = 0.95 - 0.04*(i-1);
    for Var = 1:length(tmp_plm.(Fields{i}))
        tt = [tt ' ' num2str(tmp_plm.(Fields{i})(Var), '%1.1E') ';'];
    end
    handles.Text_Fields(i) = text(px, py, [FieldsN{i} '= ' tt ], 'interpreter','latex');
    handles.Text_Fields(i).Color = 'k';
    handles.Text_Fields(i).FontSize = 12;
    handles.Text_Fields(i).FontWeight = 'bold';
end
i = i + 1;
py = 0.95 - 0.04*(i-1);

t_ky = '';
t_kx = '';
for Var = 1:VarL
    SizeR = size(results);
    if length(SizeR)>2
        Rplot = reshape(results(Var,:,:), SizeR(2:3));
    else
        Rplot = results(Var,:,:);
    end
    if length(data.ky)>1 && length(data.kx)>1
        [~, ind] = max(imag(Rplot(:)));
        [I_row, I_col] = ind2sub(size(Rplot),ind);
    else
        [~,indMax] = max(imag(results(Var,:,:)));
        if length(data.ky)>1
            I_row = 1;
            I_col = indMax;
        end
        if length(data.kx)>1
            I_row = indMax;
            I_col = 1;
        end
    end
    if length(data.ky)>1
        t_ky = [t_ky ' ' num2str(data.ky(I_col), '%1.0f') ';'];
    end
    if length(data.kx)>1
        t_kx = [t_kx ' ' num2str(data.kx(I_row), '%1.0f') ';'];
    end
    Omega(Var) = (results(Var, I_row, I_col));
end
i = i + 1;
if length(data.ky)>1
    handles.Text_Fields(i) = text(px, py, ['Max $k_y= ' t_ky '$ $m^{-1}$'],'interpreter','latex');
    py = 0.95 - 0.04*(i-1);
end
if length(data.kx)>1
    handles.Text_Fields(i) = text(px, py, ['Max $k_x= ' t_kx '$ $m^{-1}$'],'interpreter','latex');
end
handles.Text_Fields(i).Color = 'k';
handles.Text_Fields(i).FontSize = 12;
handles.Text_Fields(i).FontWeight = 'bold';
for Var = 1:VarL
%     if length(data.ky)>1 && length(data.kx)>1
%         Omega = (results(Var, I_row, I_col));
%     else
%         Omega = (results(Var,I_row, I_col));       
%     end
    i = i + 1;
    py = 0.95 - 0.04*(i-1);
    handles.Text_Fields(i) = text(px, py, ['Max $\omega= ' num2str(Omega(Var), '%1.1E') '$, $s^{-1}$'],'interpreter','latex');
    handles.Text_Fields(i).Color = 'k';
    handles.Text_Fields(i).FontSize = 12;
    handles.Text_Fields(i).FontWeight = 'bold';
end